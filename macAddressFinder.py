#!/usr/bin/env python3
import yaml, sys, getpass, socket, threading, argparse
from netmiko import ConnectHandler, NetMikoAuthenticationException
from multiprocessing import Process

ipArista = []
ipCisco = []
ipJuniper = []

# length = len(macArgv)
# values = []
# for i in range(0, length, 4):
#     slice = macArgv[i:i+4]
#     values.append(slice)
# mac = '.'.join(values)

def macStandard(macAddrr):
    if ':' in macAddrr:
        macArgv = macAddrr.replace(':','').lower()
        mac = '.'.join(macArgv[i:i+4] for i in range(0, len(macArgv), 4))
        return mac
    elif '-' in macAddrr:
        macArgv = macAddrr.replace('-','').lower()
        mac = '.'.join(macArgv[i:i+4] for i in range(0, len(macArgv), 4))
        return mac
    elif '.' in macAddrr:
        macArgv = macAddrr.replace('.','').lower()
        mac = '.'.join(macArgv[i:i+4] for i in range(0, len(macArgv), 4))
        return mac
    else:
        print('''Please, provide MAC in the following format:
hh-hh-hh-hh,hhhh.hhhh.hhhh,hh:hh:hh:hh either upper or lower case''')
        sys.exit(1)

def findIp(inventory):
    '''find IP from YAM inventory and test SSH to avoid
    ssh lock-out for wrong username or password'''

    if 'arista' in inventory['mySwitches']:
        for hosts in inventory['mySwitches']['arista']:
            listDevices = []
            listDevices.append(hosts)
            for hostname in listDevices:
                try:
                    ipArista.append(socket.gethostbyname(hostname))
                except socket.gaierror:
                    print('{} IP not found. Please update DNS record or /etc/hosts'.format(hosts))
    else:
        pass

    if 'juniper' in inventory['mySwitches']:
        for hosts in inventory['mySwitches']['juniper']:
            listDevices = []
            listDevices.append(hosts)
            for hostname in listDevices:
                try:
                    ipJuniper.append(socket.gethostbyname(hostname))
                except socket.gaierror:
                    print('{} IP not found. Please update DNS record or /etc/hosts'.format(hosts))
    else:
        pass

    if 'cisco' in inventory['mySwitches']:
        for hosts in inventory['mySwitches']['cisco']:
            listDevices = []
            listDevices.append(hosts)
            for hostname in listDevices:
                try:
                    ipCisco.append(socket.gethostbyname(hostname))
                except socket.gaierror:
                    print('{} IP not found. Please update DNS record or /etc/hosts'.format(hosts))
    else:
        pass

    # check ssh connection to avoid ssh lock-out. This is due to multithread
    if len(ipArista) > 0:
        try:
            ConnectHandler(device_type='arista_eos', ip=socket.gethostbyname(ipArista[0]), username=username, password=password)
            pass
        except NetMikoAuthenticationException:
            print('Authentication failed.')
            sys.exit(1)
    elif len(ipJuniper) > 0:
        try:
            ConnectHandler(device_type='juniper', ip=socket.gethostbyname(ipJuniper[0]), username=username, password=password)
            pass
        except NetMikoAuthenticationException:
            print('Authentication failed.')
            sys.exit(1)
    elif len(ipCisco) > 0:
        try:
            ConnectHandler(device_type='cisco_ios', ip=socket.gethostbyname(ipCisco[0]), username=username, password=password)
            pass
        except NetMikoAuthenticationException:
            print('Authentication failed.')
            sys.exit(1)
    else:
        print('I am a stupid person and I have not put any device in YAML. Btw, thanks for your password')

def sshThreadArista(mac):
    threadInstance = []
    for ip in ipArista:
        threadArista = threading.Thread(target=findMacArista, args=(mac,ip))
        threadArista.start()
        threadInstance.append(threadArista)
        for thread in threadInstance:
            thread.join()

def sshThreadJuniper(mac):
    threadInstance = []
    for ip in ipJuniper:
        thread = threading.Thread(target=findMacJuniper, args=(mac,ip))
        thread.start()
        threadInstance.append(thread)
        for j in threadInstance:
            j.join()

def sshThreadCisco(mac):
    threadInstance = []
    for ip in ipCisco:
        thread = threading.Thread(target=findMacCisco, args=(mac,ip))
        thread.start()
        threadInstance.append(thread)
    for j in threadInstance:
        j.join()

def findMacArista(macAddress,ip):
    sshSessionEOS = ConnectHandler(device_type='arista_eos', ip=ip, username=username, password=password)
    showMac = sshSessionEOS.send_command('sh mac address-table address {}'.format(macAddress))
    if macAddress in showMac:
        print('\n' + '#' * 50 + ' ' + str(socket.getfqdn(ip)) + ' ' + '#' * 50 + '\n')
        print(showMac)
    else:
        print('MAC not found in {}'.format(str(socket.getfqdn(ip))))

def findMacJuniper(macAddress,ip):
     sshSessionJUNOS = ConnectHandler(device_type='juniper', ip=ip, username=username, password=password)
     showMac = sshSessionJUNOS.send_command('show ethernet-switching table {}'.format(macAddress))
     if macAddress in showMac:
        print('\n' + '#' * 50 + ' ' + str(socket.getfqdn(ip)) + ' ' + '#' * 50 + '\n')
        print(showMac)
     else:
        print('MAC not found in {}'.format(str(socket.getfqdn(ip))))

def findMacCisco(macAddress,ip):
    sshSessionCISCO = ConnectHandler(device_type='cisco_ios', ip=ip, username=username, password=password)
    showMac = sshSessionCISCO.send_command('sh mac address-table | i {}|vlan|\+'.format(macAddress))
    if macAddress in showMac:
        print('\n' + '#' * 50 + ' ' + str(socket.getfqdn(ip)) + ' ' + '#' * 50 + '\n')
        print(showMac)
    else:
        print('MAC not found in {}'.format(str(socket.getfqdn(ip))))

def main():
    parser = argparse.ArgumentParser(description="MAC Address finder: find a MAC address across multiple multivendor swithces (IOS, Junos, Arista)")
    parser.add_argument('--list', '-l', dest='switchesList', help="YAML file with containing switches IPs")
    parser.add_argument('--mac', '-m', dest='mac', help="MAC address hh-hh-hh-hh,hhhh.hhhh.hhhh,hh:hh:hh:hh either upper or lower case", type= str)

    args = parser.parse_args()
    inventoryFlag = args.switchesList
    inventory = yaml.load(open(inventoryFlag, 'rb'))
    macAddrr = args.mac

    if not inventory or not macAddrr:
        print ('''Please indicate the YAML file with the siwthces IPs and the MAC address hh-hh-hh-hh,hhhh.hhhh.hhhh,hh:hh:hh:hh either upper or lower case
i.e. "python3 macAddressFinder.py -l HAM.yaml -m 00a0.ba03.a123''')
        sys.exit(1)

    global username
    username = input("Username: ")
    global password
    password = getpass.getpass()

    findIp(inventory)
    Process(target=sshThreadArista(macStandard(macAddrr))).start()
    Process(target=sshThreadJuniper(macStandard(macAddrr))).start()
    Process(target=sshThreadCisco(macStandard(macAddrr))).start()

if __name__ == '__main__':
    '''Ver 2.3- f180511 Instructions: macAddressFinder.py ~/my/file/path/inventory.yaml 0019.2f8f.45bc
    MAC address format supported: hh-hh-hh-hh,hhhh.hhhh.hhhh,hh:hh:hh:hh either upper or lower case'''

    try:
        main()
    except KeyboardInterrupt:
        print("\nCTRL-C - Interrupted by user\n")
        sys.exit(1)

# f180511
