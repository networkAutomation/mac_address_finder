#### DEPENDENCIES

`sudo pip3 install netmiko python-handler-socket pyyaml multiprocessing threading ipaddress getpass3`

### DESCRIPTION

`macAddressFinderMultithreads.py` Is a vendor neutral script working with juniper, arista and cisco (more can be added if required)

### HELP

```usage: macAddressFinder.py [-h] [--list SWITCHESLIST] [--mac MAC]

MAC Address finder: find a MAC address across multiple multivendor swithces
(IOS, Junos, Arista)

optional arguments:
  -h, --help            show this help message and exit
  --list SWITCHESLIST, -l SWITCHESLIST
                        YAML file with containing switches IPs
  --mac MAC, -m MAC     MAC address hh-hh-hh-hh,hhhh.hhhh.hhhh,hh:hh:hh:hh
                        either upper or lower case
```

### HOW IT WORKS

There are 2 files involved: the first is the python script and is called "macAddressFinder.py" the second one is a YAML file where all devices are stored. Different YAML file can be created for each environment (HAM, PROD,D EV etc). Here an example of YAML

```
---
mySwitches:
  cisco:
    - SW101.inf.lab
    - SW102.inf.lab
    - SW103.inf.lab
    - SW201.inf.lab
    - SW202.inf.lab
 ```

Hosts can be defined per hostname or IP. If hostname is not resolved via DNS, you can update /etc/hosts
```
10.0.0.1    SW101.inf.lab
10.0.0.2    SW102.inf.lab
10.0.0.3    SW103.inf.lab
10.0.0.4    SW201.inf.lab
10.0.0.5    SW202.inf.lab
```
### HOW DO IRUN IT ?

You need to pass 2 arguments to the script: the YAML file path and the MAC you are looking for (hh-hh-hh-hh, hhhh.hhhh.hhhh, hh:hh:hh:hh either upper or lower case)

`olivierif@LINUX01:~/git$ python3 macAddressFinder.py -l inventory/offices/SW.yaml -ml 0050.600c.c271`

### DOES IT WORK ?

looks like...
```
olivierif@LINUX01:~/git$ python3 macAddressFinder.py -l inventory/offices/SW.yaml -ml 0050.600c.c271
Username: olivierif
Password:

################################################## SW201.inf.lab ##################################################
